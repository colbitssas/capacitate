<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "registros".
 *
 * @property integer $id
 * @property integer $id_nodo
 * @property string $gas
 * @property string $fecha
 * @property string $valor
 * @property integer $enviado
 */
class Registros extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'registros';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_nodo', 'enviado'], 'integer'],
            [['gas', 'fecha', 'valor'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_nodo' => 'Id Nodo',
            'gas' => 'Gas',
            'fecha' => 'Fecha',
            'valor' => 'Valor',
            'enviado' => 'Enviado',
        ];
    }
}
