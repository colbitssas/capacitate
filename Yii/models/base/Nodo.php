<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "nodo".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $latitud
 * @property string $longitud
 */
class Nodo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nodo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'latitud', 'longitud'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'latitud' => 'Latitud',
            'longitud' => 'Longitud',
        ];
    }
}
